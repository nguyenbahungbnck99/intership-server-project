<?php

use App\Http\Controllers\Admin\AuthorController;
use App\Http\Controllers\Admin\BannerController;
use App\Http\Controllers\Admin\CategoryContentController;
use App\Http\Controllers\Admin\CategoryProductController;
use App\Http\Controllers\Admin\ContentController;
use App\Http\Controllers\Admin\ProductController;
use App\Http\Controllers\Admin\SettingController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Site\ClientController;
use App\Http\Controllers\Site\UserController as SiteUserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('/getInforWebsite', [ClientController::class,'getInforWebsite']);
Route::get('/listProductAll', [ClientController::class,'listProductAll']);
Route::get('/listProductFunctionalFoods', [ClientController::class,'listProductFunctionalFoods']);
Route::get('/listProductMilk', [ClientController::class,'listProductMilk']);
Route::get('/listSlider', [ClientController::class,'listSlider']);
Route::get('/listSlider', [ClientController::class,'listSlider']);



Route::get('/search', [ClientController::class,'search']);
Route::get('/product-detail-by-slug/{slug}', [ClientController::class,'productDetailBySlug']);
Route::get('/list-product-by-category/{category_slug}', [ClientController::class,'listProductByCategory']);
Route::get('/list-all-product', [ClientController::class,'listAllProduct']);
Route::get('/detailContent/{content_slug}', [ClientController::class,'detailContent']);
Route::get('/listContentByCategorySlug/{category_slug}', [ClientController::class,'listContentByCategorySlug']);
Route::get('/listCategoryContent', [ClientController::class,'listCategoryContent']);
Route::get('/get-new-product', [ClientController::class,'getNewProduct']);
Route::get('/list-hot-product', [ClientController::class,'listHotProduct']);
Route::get('/list-product', [ClientController::class,'listProduct']);
Route::get('/product-detail/{id}', [ClientController::class,'productDetail']);
Route::get('/getProductRelated/{id}', [ClientController::class,'getProductRelated']);

Route::get('/list-category-product', [ClientController::class,'listCategoryProduct']);


Route::prefix('site')->name('site')->group(function () {
    Route::namespace('Site')->group(function () {
        Route::post('/login', [SiteUserController::class, 'login'])->name('login');
        Route::post('/logout', [SiteUserController::class, 'logout'])->name('logout');
        Route::post('/register', [SiteUserController::class, 'register'])->name('register');
    });
});
Route::prefix('admin')->name('admin.')->group(function () {
    Route::namespace('Admin')->group(function () {
        Route::post('/login', [UserController::class, 'login'])->name('login');
        Route::post('/logout', [UserController::class, 'logout'])->name('logout');
        Route::post('/register', [UserController::class, 'register'])->name('register');

        Route::group(['middleware' => ['auth.jwt']], function () {
            Route::get('/get-user-information', [UserController::class, 'getUserInfo'])->name('getUserInfo');
            Route::post('/update-user', [UserController::class, 'updateUser'])->name('updateUser');
            Route::post('/change-password', [UserController::class, 'changePassword'])->name('changePassword');
            Route::prefix('setting')->name('setting')->group(function () {
                Route::get('/', [SettingController::class, 'index'])->name('index');
                Route::post('/store', [SettingController::class, 'store'])->name('store');
            });
            Route::prefix('categoryContent')->name('categoryContent')->group(function () {
                Route::get('/listAll', [CategoryContentController::class, 'listAll'])->name('listAll');
                Route::get('/', [CategoryContentController::class, 'index'])->name('index');
                Route::get('/edit/{id}', [CategoryContentController::class, 'edit'])->name('edit');
                Route::post('/create', [CategoryContentController::class, 'create'])->name('create');
                Route::put('/update/{id}', [CategoryContentController::class, 'update'])->name('update');
                Route::delete('/delete/{id}', [CategoryContentController::class, 'delete'])->name('delete');
            });
            Route::prefix('content')->name('content.')->group(function () {
                Route::get('/listAll', [ContentController::class, 'listAll'])->name('listAll');
                Route::get('/', [ContentController::class, 'index'])->name('index');
                Route::get('/edit/{id}', [ContentController::class, 'edit'])->name('edit');
                Route::post('/create', [ContentController::class, 'create'])->name('create');
                Route::put('/update/{id}', [ContentController::class, 'update'])->name('update');
                Route::delete('/delete/{id}', [ContentController::class, 'delete'])->name('delete');
            });
            Route::prefix('categoryProduct')->name('categoryProduct')->group(function () {
                Route::get('/listAll', [CategoryProductController::class, 'listAll'])->name('listAll');
                Route::get('/', [CategoryProductController::class, 'index'])->name('index');
                Route::post('/create', [CategoryProductController::class, 'create'])->name('create');
                Route::put('/update/{id}', [CategoryProductController::class, 'update'])->name('update');
                Route::delete('/delete/{id}', [CategoryProductController::class, 'delete'])->name('delete');
            });
            Route::prefix('product')->name('product.')->group(function () {
                Route::get('/', [ProductController::class, 'index'])->name('index');
                Route::post('/create', [ProductController::class, 'create'])->name('create');
                Route::get('/edit/{id}', [ProductController::class, 'edit'])->name('edit');
                Route::put('/update/{id}', [ProductController::class, 'update'])->name('update');
                Route::delete('/delete/{id}', [ProductController::class, 'delete'])->name('delete');
            });

            Route::prefix('banner')->name('banner.')->group(function () {
                Route::get('/', [BannerController::class, 'index'])->name('index');
                Route::post('/create', [BannerController::class, 'create'])->name('create');
                Route::get('/edit/{id}', [BannerController::class, 'edit'])->name('edit');
                Route::put('/update/{id}', [BannerController::class, 'update'])->name('update');
                Route::put('/active/{id}', [BannerController::class, 'active'])->name('active');
                Route::delete('/delete/{id}', [BannerController::class, 'delete'])->name('delete');
            });
            Route::prefix('author')->name('author.')->group(function () {
                Route::get('/', [AuthorController::class, 'index'])->name('index');
                Route::get('/listAuthorAll', [AuthorController::class, 'listAuthorAll'])->name('listAuthorAll');
                Route::post('/create', [AuthorController::class, 'create'])->name('create');
                Route::get('/edit/{id}', [AuthorController::class, 'edit'])->name('edit');
                Route::put('/update/{id}', [AuthorController::class, 'update'])->name('update');
                Route::delete('/delete/{id}', [AuthorController::class, 'delete'])->name('delete');
            });
        });
    });
});
