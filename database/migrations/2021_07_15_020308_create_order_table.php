<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->integer('total_product')->nullable();
            $table->float('total_money_product')->nullable()->default(0);
            $table->float('ship')->nullable();
            $table->integer('status')->nullable()->default(1);
            $table->string('content')->nullable();
            $table->string('name_user')->nullable();
            $table->string('address_order')->nullable();
            $table->string('phone_order')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order');
    }
}
