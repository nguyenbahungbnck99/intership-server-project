<?php

use App\Helpers\Helpers;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateUpdateSlugProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       $listProduct = DB::table('product')->get();
       foreach($listProduct as $product){
           $slug = Helpers::text_to_slug($product->name);

           DB::table('product')->where('id',$product->id)->update(['slug'=> $slug]);
       }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */

}
