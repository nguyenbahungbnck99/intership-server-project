<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateUpdateRoleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $insert = [
            [
                'name' => 'Content',
            ],
            [
                'name' => 'Sale',
            ],
        ];
        DB::table('roles')->insert($insert);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */

}
