<?php

use App\Helpers\Helpers;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateUpdateCategoryContentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('category', function (Blueprint $table) {
            $table->string('slug')->nullable()->after('name');
        });
        $listCategory = DB::table('category')->get();
        foreach ($listCategory as $category) {
            $slug = Helpers::text_to_slug($category->name);
            $update = ['slug' => $slug];
            DB::table('category')->where('id', $category->id)->update($update);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */

}
