<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWithdrawalHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('withdrawal_histories', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('withdrawal_wallet_id');
            $table->string('account_name');
            $table->string('account_stk');
            $table->integer('money');
            $table->text('content')->nullable();
            $table->string('image');
            $table->integer('status')->default(0);
            $table->timestamps();
            $table->foreign('withdrawal_wallet_id','frk_withdrawal')->references('id')->on('withdrawal_wallets');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('withdrawal_histories');
    }
}
