<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->unsignedInteger('role_id')->default(2);
            $table->foreign('role_id')->references('id')->on('roles');
            $table->string('email')->nullable()->unique();
            $table->string('phone')->unique();
            $table->string('cmt')->nullable()->unique();
            $table->string('address')->nullable();
            $table->string('password');
            $table->integer('code_branch')->nullable();
            $table->integer('code_ordinal')->nullable();
            $table->integer('status')->default(1);
            $table->string('device_id')->nullable();
            $table->integer('cart_total_product')->nullable()->default(0);
            $table->float('cart_total_money')->nullable()->default(0);
            $table->rememberToken();
            $table->timestamps();
        });
        DB::table('users')->insert([
            [
                'name' => "Admin",
                'email' => "bahung2108@gmail.com",
                'role_id' => 1,
                'phone' => "0972218408",
                'cmt' => "125877630",
                'address' => "Hà Nội",
                'password' => Hash::make("123456"),
                'code_branch' => null,
                'code_ordinal' => null,
                'cart_total_product' => null,
                'cart_total_money' => null
            ],
            [
                'name' => "Hùng bá",
                'email' => "nguyenbahungbnck99@gmail.com",
                'role_id' => 2,
                'phone' => "0961578794",
                'cmt' => "0961578794",
                'address' => "Hà Nội",
                'password' => Hash::make("123456"),
                'code_branch' => null,
                'code_ordinal' => null,
                'cart_total_product' => null,
                'cart_total_money' => null
            ]

        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
