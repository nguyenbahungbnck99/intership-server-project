<?php

use App\Helpers\Helpers;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateUpdateContent2Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contents', function (Blueprint $table) {
            $table->string('slug')->nullable()->after('title');
        });
        $listContent = DB::table('contents')->get();
        foreach($listContent as $content){
            $slug = Helpers::text_to_slug($content->title);
            $update = ['slug' => $slug];
            DB::table('contents')->where('id',$content->id)->update($update);
        }
    }


}
