<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUpdateDetailCartTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('detail_cart', function($table){
            $table->text('product_name')->nullable();
            $table->dropForeign('detail_cart_product_id_foreign');
            $table->foreign('product_id')->references('id')->on('product')->onDelete('cascade');
            $table->dropForeign('detail_cart_user_id_foreign');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    
}
