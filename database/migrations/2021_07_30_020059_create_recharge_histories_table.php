<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRechargeHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recharge_histories', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('recharge_wallet_id');
            $table->string('account_name');
            $table->string('account_stk');
            $table->string('image');
            $table->integer('money');
            $table->text('content')->nullable();
            $table->timestamps();
            $table->foreign('recharge_wallet_id','frk_recharge')->references('id')->on('recharge_wallets')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recharge_histories');
    }
}
