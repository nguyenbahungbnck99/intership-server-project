<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUpdateProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product', function (Blueprint $table) {
            $table->unsignedInteger('category_id');
            
            $table->integer('total_inventory')->nullable()->default(0);
            $table->integer('total_sold')->nullable()->default(0);
            $table->text('content')->nullable();
            $table->string('slug')->nullable();
            $table->text('user_object')->nullable();
            $table->text('user_manual')->nullable();
            $table->text('usefulness')->nullable();
            $table->foreign('category_id')->references('id')->on('category_products');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
}
