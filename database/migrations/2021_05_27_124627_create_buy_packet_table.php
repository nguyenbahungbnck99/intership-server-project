<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBuyPacketTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('buy_packet', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->unsignedInteger('packet_id');
            $table->foreign('packet_id')->references('id')->on('packet');
            $table->unsignedInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->BigInteger('price')->default(0);
            $table->string('phone')->nullable();
            $table->string('address')->nullable();
            $table->text('content')->nullable();
            $table->String('code_ctv')->nullable();
            $table->String('content_payment')->nullable();
            $table->integer('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('buy_packet');
    }
}
