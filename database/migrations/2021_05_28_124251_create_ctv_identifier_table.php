<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCtvIdentifierTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ctv_identifier', function (Blueprint $table) {
            $table->increments('id');
            $table->String('name')->nullable();
            $table->BigInteger('price')->default(0);
            $table->integer('share_number')->default(0);
            $table->BigInteger('money')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ctv_identifier');
    }
}
