<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product', function (Blueprint $table) {
            $table->increments('id');
            $table->String('image')->nullable();
            $table->String('name')->nullable();
            $table->float('price')->nullable()->default(0);
            $table->float('price_sale')->nullable();
            $table->Text('description')->nullable();
            $table->Text('strategic_vision')->nullable();
            $table->LongText('description')->nullable()->change();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product');
    }
}
