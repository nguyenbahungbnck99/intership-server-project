<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRewardHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reward_history', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('reward_wallet_id');
            $table->string('account_name');
            $table->integer('money');
            $table->text('content')->nullable();
            $table->string('image');
            $table->integer('status')->default(0);
            $table->integer('type');
            $table->unsignedInteger('order_id')->nullable();
            $table->foreign('order_id')->references('id')->on('order');
            $table->timestamps();
            $table->foreign('reward_wallet_id','frk_reward')->references('id')->on('reward_wallet');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reward_history');
    }
}
