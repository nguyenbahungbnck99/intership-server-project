<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDetailContent2Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_content2_product', function (Blueprint $table) {
            $table->increments('id');
            $table->text('content')->nullable();
            $table->unsignedInteger('content_id');
            $table->foreign('content_id')->references('id')->on('content2product');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_content2');
    }
}
