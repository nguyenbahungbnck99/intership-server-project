<?php

namespace App\Traits;

use Illuminate\Http\JsonResponse;
use Illuminate\Support\Arr;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\User;
use App\Http\Utils\SystemParam;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Tymon\JWTAuth\Facades\JWTAuth;

trait GetAuthenticatedUser
{

    /**
     * Parse response format
     *
     * @param  array $data
     * @param  string $statusCode
     * @return JsonResponse
     */
    public function getAuthenticatedUser()
    {
        try {
            if (!$user = JWTAuth::parseToken()->authenticate()) {
                return null;
                // return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::user_not_found, null);
            }
        } catch (TokenExpiredException $e) {
            return null;
            // return $this->responseApi(SystemParam::status_error, $e->getStatusCode(), SystemParam::token_expired, null);
        } catch (TokenInvalidException $e) {
            return null;
            // return $this->responseApi(SystemParam::status_error, $e->getStatusCode(),  SystemParam::token_invalid, null);
        } catch (JWTException $e) {
            return null;
            // return $this->responseApi(SystemParam::status_error, $e->getStatusCode(),  SystemParam::token_absent, null);
        }
        return $user;
    }
}
