<?php

namespace App\Http\Middleware;

use Closure;
use Exception;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;

class VerifyJWTToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        try {
            $user = JWTAuth::parseToken()->authenticate();
            if ($user->role_id != 1) {
                return response()->json([
                    'status' => 0,
                    'code' => 502,
                    'message' => 'Tài khoản của bạn không có quyền truy cập.',
                    'data' => null
                ]);
            }
        } catch (Exception $e) {
            if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenInvalidException) {
                return response()->json([
                    'status' => 0,
                    'code' => 502,
                    'message' => 'Token không hợp lệ',
                    'data' => null
                ]);
            } else if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenExpiredException) {
                return response()->json([
                    'status' => 0,
                    'code' => 502,
                    'message' => 'Token đã hết hạn',
                    'data' => null
                ]);
            } else {
                return response()->json([
                    'status' => 0,
                    'code' => 502,
                    'message' => 'Không tìm thấy token',
                    'data' => null
                ]);
            }
        }
        return $next($request);
    }
}
