<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RequestLogin extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'phone' => 'required|regex:/^0[0-9]{9,11}+$/|unique:users',
            'password' => 'required',
        ];
    }
    public function messages()
    {
        return [
            'phone.required' => 'Số điện thoại chưa được nhập!',
            'phone.regex' => 'Số điện thoại không đúng định dạng!',
            'phone.unique' => 'Số điện thoại đã được đăng ký!',
            'name.required' => 'Tên chưa được nhập!',
            'password.required' => 'Mật khẩu chưa được nhập!',
        ];
    }
}
