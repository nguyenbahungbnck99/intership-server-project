<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RequestRegisterCustomer extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'phone' => 'required|regex:/^0[0-9]{9,11}+$/|unique:users',
            'email' => 'required|email|unique:users',
            'name' => 'required|string|max:50',
            'password' => 'required',
            'confirm_password' => 'required|same:password',
        ];
    }
    public function messages()
    {
        return [
            'phone.required' => 'Số điện thoại chưa được nhập!',
            'phone.regex' => 'Số điện thoại không đúng định dạng!',
            'phone.unique' => 'Số điện thoại đã được đăng ký!',
            'email.required' => 'Email chưa được nhập!',
            'email.unique' => 'Email đã được đăng ký!',
            'email.email' => 'Email không đúng định dạng',
            'name.required' => 'Tên chưa được nhập!',
            'password.required' => 'Mật khẩu chưa được nhập!',
            'confirm_password.required' => 'Xác nhận mật khẩu chưa được nhập!',
            'confirm_password.same' => 'Xác nhận mật khẩu không trùng khớp!',
        ];
    }
    public function filters()
    {
        return [
            'email' => 'trim|lowercase',
            'name' => 'trim|capitalize|escape'
        ];
    }
}
