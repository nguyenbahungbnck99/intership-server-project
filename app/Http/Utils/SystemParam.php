<?php

namespace App\Http\Utils;

use Illuminate\Support\Facades\File;

class SystemParam
{

    const PAGE_NUMBER = 12;
    const status_success = 1;
    const status_error = 0;
    const code_success = 200;
    const code_create_success = 201;
    const code_delete_success = 204;
    const code_no_content = 205;
    const code_not_modify = 304;
    const code_bad_request = 400;
    const code_forbidden = 403;
    const code_not_found = 404;
    const code_method_not_allowed = 405;
    const code_gone = 410;
    const code_unprocessable_entity = 410;
    const code_error_validate = 422;
    const code_too_many_request = 429;
    const code_error_server = 501;
    const code_unauthorized = 401;
    const code_error_already_exist = 499;
    const too_many_request = "Request bị từ chối do bị giới hạn";
    const error_validate = "Dữ liệu không được xác thực";
    const unprocessable_entity = "Không hỗ trợ kiểu Resource này";
    const gone = "Resource không còn tồn tại";
    const method_not_allowed = "Phương thức không cho phép với user hiện tại";
    const not_found = "Không tìm thấy resource từ URI";
    const forbidden = "Bị từ chối không cho phép";
    const unauthorized = "Yêu cầu cần có auth";
    const bad_request = "Yêu cầu không hợp lệ";
    const login_success = "Đăng nhập thành công";
    const message_login_user_not_found = "Không tìm thấy tài khoản";
    const message_login_error_status = "Tài khoản không hoạt động";
    const message_account_locked = "Tài khoản đã bị khóa";
    const message_account_delete = "Tài khoản đã bị xóa";
    const register_recruit_success = "Đăng ký thành công! Vui lòng kiểm tra lại email để xác nhận đăng ký";
    const register_success = "Đăng ký thành công";
    const register_error = "Đăng ký thất bại";
    const success = "Thành công";
    const create_success = "Thêm mới thành công";
    const update_success = "Cập nhật thành công";
    const delete_success = "Xóa thành công";
    const logout_success = "Đăng xuất thành công";
    const create_error = "Thêm mới thất bại";
    const update_error = "Cập nhật thất bại";
    const delete_error = "Xóa thất bại";
    const token_expired = "Token đã hết hạn sử dụng";
    const error_not_role = "Bạn không có quyền truy cập";
    const not_found_category = "Không tìm thấy danh mục khóa học";
    const not_found_author = "Không tìm thấy tác giả";
    const not_found_course = "Không tìm thấy khóa học";
    const not_found_lession = "Không tìm thấy bài học";
    const not_found_gift = "Không tìm thấy quà tặng";
    const not_found_tool = "Không tìm thấy công cụ";
    const not_found_question = "Không tìm thấy câu hỏi";
    const not_found_combo = "Không tìm thấy combo";
    const not_found_comboCourse = "Không tìm thấy combo khóa học";
    const message_login_error = "Đăng nhập không thành công";
    const update_password_success = "Cập nhật mật khẩu thành công";
    const error_email_already_exist = "Email đã tồn tại trong hệ thống";
    const error_already_exist = "Bản ghi tồn tại trong hệ thống";


    public static function text_to_slug($str)
    {
        $str = trim(mb_strtolower($str));
        $str = preg_replace('/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/', 'a', $str);
        $str = preg_replace('/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/', 'e', $str);
        $str = preg_replace('/(ì|í|ị|ỉ|ĩ)/', 'i', $str);
        $str = preg_replace('/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/', 'o', $str);
        $str = preg_replace('/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/', 'u', $str);
        $str = preg_replace('/(ỳ|ý|ỵ|ỷ|ỹ)/', 'y', $str);
        $str = preg_replace('/(đ)/', 'd', $str);
        $str = preg_replace('/[^a-z0-9-\s]/', '', $str);
        $str = preg_replace('/([\s]+)/', '-', $str);
        return $str;
    }
    public static function saveImageFolder($image, $folder)
    {
        $fileName = uniqid() . "." . $image->getClientOriginalExtension();
        $image->move($folder, $fileName);
        return $folder . $fileName;
    }
    public static function saveImage($image, $folder)
    {
        $fileName = uniqid() . "." . $image->getClientOriginalExtension();
        $image->move($folder, $fileName);
        return $folder . $fileName;
    }
    public static function cleanString($string)
    {
        return preg_replace("/[^A-Za-z0-9]/", "", $string);
    }
    public static function deleteImage($linkImage)
    {
        if (File::exists(public_path($linkImage))) {
            File::delete(public_path($linkImage));
        }
    }
}
