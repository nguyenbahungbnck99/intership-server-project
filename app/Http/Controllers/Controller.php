<?php

namespace App\Http\Controllers;

use App\Traits\ApiResponse;
use App\Traits\GetAuthenticatedUser;
use App\Traits\GoogleApi;
use App\Traits\MakeCodeRandom;
use App\Traits\UploadImage;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests, ApiResponse, GetAuthenticatedUser, UploadImage, MakeCodeRandom, GoogleApi;
}
