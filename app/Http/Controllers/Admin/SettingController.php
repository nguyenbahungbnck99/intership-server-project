<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Utils\SystemParam;
use App\Models\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SettingController extends Controller
{
    //
    public function index()
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 1) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $infor = Setting::getInfor();
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $infor);
    }
    public function store(Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 1) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $data = $request->only(
            'email',
            'phone',
            'fb_ref',
            'page_id',
            'introduce',
            'copyright',
            'logo',
            'address',
        );
        try {
            //code...

            foreach ($data as $key => $value) {
                if ($key == 'phone') {
                    $phone = Setting::where('type', Setting::TYPE_PHONE)->first();
                    if ($phone) {
                        $create = [
                            'type' => Setting::TYPE_PHONE,
                            'content' => $value
                        ];
                        $phone->update($create);
                    } else {
                        $create = [
                            'type' => Setting::TYPE_PHONE,
                            'content' => $value
                        ];
                        Setting::create($create);
                    }
                }
                if ($key == 'email') {
                    $email = Setting::where('type', Setting::TYPE_EMAIL)->first();
                    if ($email) {
                        $create = [
                            'type' => Setting::TYPE_EMAIL,
                            'content' =>  $value
                        ];
                        $email->update($create);
                    } else {
                        $create = [
                            'type' => Setting::TYPE_EMAIL,
                            'content' =>  $value
                        ];
                        Setting::create($create);
                    }
                }
                if ($key == 'address') {
                    $address = Setting::where('type', Setting::TYPE_ADDRESS)->first();
                    if ($address) {
                        $create = [
                            'type' => Setting::TYPE_ADDRESS,
                            'content' =>  $value
                        ];
                        $address->update($create);
                    } else {
                        $create = [
                            'type' => Setting::TYPE_ADDRESS,
                            'content' =>  $value
                        ];
                        Setting::create($create);
                    }
                }
                if ($key == 'copyright') {
                    $copyright = Setting::where('type', Setting::TYPE_COPYRIGHT)->first();
                    if ($copyright) {
                        $create = [
                            'type' => Setting::TYPE_COPYRIGHT,
                            'content' =>  $value
                        ];
                        $copyright->update($create);
                    } else {
                        $create = [
                            'type' => Setting::TYPE_COPYRIGHT,
                            'content' =>  $value
                        ];
                        Setting::create($create);
                    }
                }
                if ($key == 'introduce') {
                    $introduce = Setting::where('type', Setting::TYPE_INTRODUCE)->first();
                    if ($introduce) {
                        $create = [
                            'type' => Setting::TYPE_INTRODUCE,
                            'content' =>  $value
                        ];
                        $introduce->update($create);
                    } else {
                        $create = [
                            'type' => Setting::TYPE_INTRODUCE,
                            'content' =>  $value
                        ];
                        Setting::create($create);
                    }
                }
                if ($key == 'fb_ref') {
                    $fb_ref = Setting::where('type', Setting::TYPE_FB_REF)->first();
                    if ($fb_ref) {
                        $create = [
                            'type' => Setting::TYPE_FB_REF,
                            'content' =>  $value
                        ];
                        $fb_ref->update($create);
                    } else {
                        $create = [
                            'type' => Setting::TYPE_FB_REF,
                            'content' =>  $value
                        ];
                        Setting::create($create);
                    }
                }
                if ($key == 'page_id') {
                    $page_id = Setting::where('type', Setting::TYPE_FB_PAGE_ID)->first();
                    if ($page_id) {
                        $create = [
                            'type' => Setting::TYPE_FB_PAGE_ID,
                            'content' =>  $value
                        ];
                        $page_id->update($create);
                    } else {
                        $create = [
                            'type' => Setting::TYPE_FB_PAGE_ID,
                            'content' =>  $value
                        ];
                        Setting::create($create);
                    }
                }
                if ($key == 'logo') {
                    if ($request->hasFile('logo')) {

                        $logo = Setting::where('type', Setting::TYPE_LOGO)->first();
                        if ($logo) {
                            $logoQuery = DB::table('settings')->where('type', Setting::TYPE_LOGO)->first();
                            SystemParam::deleteImage($logoQuery->content);

                            $filePath = SystemParam::saveImageFolder($request->logo, Setting::FOLDER);
                            $create = [
                                'type' => Setting::TYPE_LOGO,
                                'content' => $filePath
                            ];
                            $logo->update($create);
                        } else {
                            $filePath = SystemParam::saveImageFolder($request->logo, Setting::FOLDER);
                            $create = [
                                'type' => Setting::TYPE_LOGO,
                                'content' => $filePath
                            ];
                            Setting::create($create);
                        }
                    }
                }
            }

            return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::update_success, '');
        } catch (\Throwable $th) {
            //throw $th;
            return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::update_error, $th->getMessage());

        }
    }
}

