<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Utils\SystemParam;
use App\Models\CategoryProduct;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CategoryProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data = CategoryProduct::with('children')->paginate(SystemParam::PAGE_NUMBER);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::success, $data);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function listAll()
    {
        //
        $data = CategoryProduct::where('parent',0)->with('children')->get();
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::success, $data);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
        try {
            //code...

            $validator = Validator::make($request->all(), [
                'name' => 'required',
            ], [
                'name.required' => 'Vui lòng nhập tên danh mục',

            ]);
            if ($validator->fails()) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
            }
            $req = $request->all();
            $where = [
                'name' => $request->get('name'),
            ];
            $where[] = ['parent', $request->parent];
            $checkCate = CategoryProduct::where($where)->first();
            if ($checkCate) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_already_exist, SystemParam::error_already_exist, '');
            }
            $req['slug'] = SystemParam::text_to_slug($request->name);
            $create = CategoryProduct::create($req);
            if ($create) {
                return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::create_success, $create);
            }
        } catch (\Throwable $th) {
            //throw $th;
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::create_error, '');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        try {
            //code...

            $validator = Validator::make($request->all(), [
                'name' => 'required',
            ], [
                'name.required' => 'Vui lòng nhập tên danh mục',

            ]);
            if ($validator->fails()) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
            }
            $req = $request->all();

            $category = CategoryProduct::find($id);
            $req['slug'] = SystemParam::text_to_slug($request->name);
            $category->update($req);
            return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::update_success, $category);
        } catch (\Throwable $th) {
            //throw $th;
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::create_error, '');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        //
        try {
            //code...

            $category = CategoryProduct::find($id);
            if ($category) {
                $category->delete();
                return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::delete_success, $category);
            }
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_already_exist, SystemParam::error_already_exist, "");
        } catch (\Throwable $th) {
            //throw $th;
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::delete_error, $th->getMessage());
        }
    }
}
