<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Utils\SystemParam;
use App\Models\Author;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class AuthorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 1) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $data = Author::paginate(20);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function listAuthorAll()
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 1) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $data = Author::all();
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 1) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'description' => 'required',
            'avatar' => 'required',
        ], [
            'name.required' => 'Vui lòng nhập tên khóa học',
            'description.required' => 'Vui lòng nhập mô tả',
            'avatar.required' => 'Vui lòng chọn ảnh',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        $data = $request->all();
        if ($request->hasFile('avatar')) {
            $filePath = SystemParam::saveImage($request->avatar, Author::FOLDER_AUTHOR);

            $data['avatar'] = $filePath;
        }
        $data['slug'] = SystemParam::text_to_slug($request->name);
        $create = Author::create($data);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::create_success, $create);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function detail($id)
    {
        //
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 1) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $author = Author::find($id);

        if (!$author) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::not_found_author, null);
        }
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $author);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 1) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $author = Author::find($id);
        if (!$author) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::not_found_author, null);
        }
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'description' => 'required',
        ], [
            'name.required' => 'Vui lòng nhập tên khóa học',
            'description.required' => 'Vui lòng nhập mô tả',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        $data = $request->all();
        if ($request->hasFile('avatar')) {
            $authQuery = DB::table('author')->where('id', $id)->first();
            SystemParam::deleteImage($authQuery->avatar);
            $filePath = SystemParam::saveImage($request->avatar, Author::FOLDER_AUTHOR);

            $data['avatar'] = $filePath;
        }
        $data['slug'] = SystemParam::text_to_slug($request->name);
        $author->update($data);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::update_success, $author);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        //
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 1) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $author = Author::find($id);
        if (!$author) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::not_found_author, null);
        }
        $authQuery = DB::table('author')->where('id', $id)->first();
        SystemParam::deleteImage($authQuery->avatar);
        $author->delete();
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::delete_success, $author);

    }
}
