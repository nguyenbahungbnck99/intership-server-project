<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Utils\SystemParam;
use App\Models\Course;
use App\Models\Lesson;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class LessionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 1) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $course = Course::find($request->course_id);
        if (!$course) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::not_found_course, null);
        }
        // return $request->course_id;
        $data = Lesson::where('course_id', $request->course_id)->get();
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 1) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'video' => 'required',
        ], [
            'name.required' => 'Vui lòng nhập tên danh mục',
            'video.required' => 'Vui lòng nhập video',
        ]);
        $course = Course::find($request->course_id);
        if (!$course) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::not_found_course,'');
        }
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        $req = $request->all();
        $req['slug'] = SystemParam::text_to_slug($request->name);
        $create = Lesson::create($req);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::create_success, $create);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function detail($id)
    {
        //
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 1) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $data = Lesson::find($id);

        if (!$data) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::not_found_lession, null);
        }
        // return $request->course_id;
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 1) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'video' => 'required',
        ], [
            'name.required' => 'Vui lòng nhập tên danh mục',
            'video.required' => 'Vui lòng nhập video',
        ]);

        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        // $course = Course::find($request->course_id);
        // if (!$course) {
        //     return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::not_found_course,'');
        // }
        $lession = Lesson::find($id);
        if (!$lession) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::not_found_lession,'');
        }
        // return $lession;
        $req = $request->all();
        $req['slug'] = SystemParam::text_to_slug($request->name);
        $lession->update($req);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::update_success, $lession);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        //
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 1) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $lession = Lesson::find($id);
        if (!$lession) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::not_found_author, null);
        }
        $lession->delete();
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $lession);
    }
}
