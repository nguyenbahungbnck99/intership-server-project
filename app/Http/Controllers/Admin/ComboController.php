<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Utils\SystemParam;
use App\Models\Combo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class ComboController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 1) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $data = Combo::with('courses')->paginate(12);
        foreach ($data as $item) {
            $item->arrayCourseId = $item['courses']->pluck('id');
        }
        // return $data[0]['courses']->pluck('id');
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
        try {
            //code...

            $user = $this->getAuthenticatedUser();
            if (!$user) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
            }
            if ($user->role_id != 1) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
            }
            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'price' => 'required',
                'image' => 'required',
                'course_id' => 'required',
            ], [
                'name.required' => 'Vui lòng nhập tên khóa học',
                'price.required' => 'Vui lòng nhập giá',
                'image.required' => 'Vui lòng chọn ảnh',
                'course_id.required' => 'Vui lòng chọn khoá học',
            ]);
            if ($validator->fails()) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
            }
            DB::beginTransaction();
            $data = $request->all();
            $data['slug'] = SystemParam::text_to_slug($request->name);
            $data['price'] = SystemParam::cleanString($request->price);

            if ($request->hasFile('image')) {
                $filePath = SystemParam::saveImage($request->image, Combo::FOLDER);
                $data['image'] = $filePath;
            }
            $create = Combo::create($data);
            $create->courses()->attach($request->course_id);
            DB::commit();
            return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::create_success, $create->load('courses'));
        } catch (\Throwable $th) {
            //throw $th;
            DB::rollback();
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::create_error, $th->getMessage());
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function detail($id)
    {
        //
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 1) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $course = Combo::find($id)->load('comboCourse', 'courses');
        if (!$course) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::not_found_course, null);
        }
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $course);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $courseId=   json_decode($request->course_id);

        try {
            //code...

            $user = $this->getAuthenticatedUser();
            if (!$user) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
            }
            if ($user->role_id != 1) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
            }
            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'price' => 'required',
                // 'image' => 'required',
            ], [
                'name.required' => 'Vui lòng nhập tên khóa học',
                'price.required' => 'Vui lòng nhập giá',
                // 'image.required' => 'Vui lòng chọn ảnh',
            ]);
            if ($validator->fails()) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
            }
            $combo = Combo::find($id);
            if (!$combo) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::not_found_combo, '');
            }
            DB::beginTransaction();
            $data = $request->all();
            if ($request->hasFile('image')) {
                $comboQuery = DB::table('combo')->where('id', $id)->first();
                SystemParam::deleteImage($comboQuery->image);
                $filePath = SystemParam::saveImage($request->image, Combo::FOLDER);
                $data['image'] = $filePath;
            }
            $data['slug'] = SystemParam::text_to_slug($request->name);
            $data['price'] = SystemParam::cleanString($request->price);
            $combo->courses()->detach();
            $combo->update($data);
            $combo->courses()->attach($courseId);
            DB::commit();
            return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::update_success, $combo->load('courses'));
        } catch (\Throwable $th) {
            //throw $th;
            DB::rollback();
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::update_error, $th->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        //
        try {
            //code...

            $user = $this->getAuthenticatedUser();
            if (!$user) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
            }
            if ($user->role_id != 1) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
            }
            $combo = Combo::find($id);
            if (!$combo) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::not_found_combo, null);
            }
            $comboQuery = DB::table('combo')->where('id', $id)->first();
            SystemParam::deleteImage($comboQuery->image);
            $combo->delete();
            return $this->responseApi(SystemParam::status_success, SystemParam::code_success,SystemParam::delete_success , $combo);
        } catch (\Throwable $th) {
            //throw $th;
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::delete_error, '');
        }
    }
}
