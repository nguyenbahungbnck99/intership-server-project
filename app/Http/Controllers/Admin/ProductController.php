<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Utils\SystemParam;
use App\Models\CategoryProduct;
use App\Models\Product;
use App\Models\ProductImage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $where = [];
        if ($request->category_id) {
            $where[] = ['category_id', $request->category_id];
        }
        if ($request->name) {
            $where[] = ['name', 'like', "%$request->name%"];
        }
        $data = Product::where($where)->with('productImage', 'categoryProduct')->paginate(SystemParam::PAGE_NUMBER);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::success, $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
        $user = $this->getAuthenticatedUser();
        $validator = Validator::make(
            $request->all(),
            [
                'name' => 'required',
                'price' => 'required',
                'image' => 'required',
                'category_id' => 'required',
                'description' => 'required',
            ],
            [
                'name.required' => 'Vui lòng nhập tên sản phẩm',
                'price.required' => 'Vui lòng nhập gía sản phẩm',
                'image.required' => 'Vui lòng chọn ảnh',
                'category_id.required' => 'Vui lòng chọn chọn danh mục sản phẩm',
                'description.required' => 'Vui lòng nhập mô tả',
            ]
        );
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        try {
            //code...

            $data = $request->all();
            $data['slug'] = SystemParam::text_to_slug($request->name);
            if ($request->hasFile('image')) {
                $filePath = SystemParam::saveImage($request->image, Product::FOLDER);
                $data['image'] = $filePath;
            }
            $data['price'] = SystemParam::cleanString($request->price);
            if ($request->price_sale) {
                $data['price_sale'] = SystemParam::cleanString($request->price_sale);
            }
            if ($request->total_inventory) {
                $data['total_inventory'] = SystemParam::cleanString($request->total_inventory);
            }
            DB::beginTransaction();
            $create = Product::create($data);
            if ($request->hasFile('list_images')) {
                foreach ($request->list_images as $image) {
                    $filePath = SystemParam::saveImage($image, Product::FOLDER);
                    $productImage = new ProductImage();
                    $productImage->product_id = $create->id;
                    $productImage->image = $filePath;
                    $productImage->save();
                }
            }
            DB::commit();

            return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::create_success, $create->load('productImage'));
        } catch (\Throwable $th) {
            //throw $th;
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::create_error,  $th->getMessage());
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //

        $data = Product::findOrFail($id)->load('productImage', 'categoryProduct');
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::success, $data);
    }

    /**delete the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $user = $this->getAuthenticatedUser();

        $validator = Validator::make(
            $request->all(),
            [
                'name' => 'required',
                'price' => 'required',
                // 'image' => 'required',
                'category_id' => 'required',
                'description' => 'required',
            ],
            [
                'name.required' => 'Vui lòng nhập tên sản phẩm',
                'price.required' => 'Vui lòng nhập gía sản phẩm',
                // 'image.required' => 'Vui lòng chọn ảnh',
                'category_id.required' => 'Vui lòng chọn chọn danh mục sản phẩm',
                'description.required' => 'Vui lòng nhập mô tả',
            ]
        );
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        try {
            //code...

            $category = CategoryProduct::find($request->category_id);
            if ($category) {
                $content = Product::find($id);


                if ($content) {
                    $req = $request->all();
                    $req['slug'] = SystemParam::text_to_slug($request->name);
                    $req['price'] = SystemParam::cleanString($request->price);
                    if ($request->price_sale) {
                        $req['price_sale'] = SystemParam::cleanString($request->price_sale);
                    }
                    if ($request->hasFile('image')) {
                        $imageQuert = DB::table('product')->where('id', $id)->first();
                        SystemParam::deleteImage($imageQuert->image);
                        $filePath = SystemParam::saveImage($request->image, Product::FOLDER);
                        $req['image'] = $filePath;
                    }
                    if ($request->hasFile('list_image')) {
                        $listImage = DB::table('list_image')->where('content_id', $id)->get();
                        foreach ($listImage as $image) {
                            SystemParam::deleteImage($image->image);
                        }
                        $content->productImage()->delete();
                        foreach ($request->image as $image) {
                            $filePath = SystemParam::saveImage($image, Product::FOLDER);
                            $postImage = [];
                            $postImage['image'] = $filePath;
                            $postImage['content_id'] = $content->id;
                            ProductImage::create($postImage);
                        }
                    }
                    $content->update($req);
                    return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::update_success, $content->load('productImage'));
                }
            }
        } catch (\Throwable $th) {
            //throw $th;
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::update_error,  $th->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        //
        try {
            //code...

            $content = Product::find($id);



            if (!$content) {
                return $this->responseApi(SystemParam::status_success, SystemParam::code_gone, SystemParam::gone, "");
            }
            $listImage = DB::table('image_product')->where('product_id', $id)->get();
            foreach ($listImage as $image) {
                SystemParam::deleteImage($image->image);
            }
            $content->productImage()->delete();
            $content->viewProductShare()->delete();
            $content->delete();
            return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::delete_success, $content);
        } catch (\Throwable $th) {
            //throw $th;
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::delete_error,  $th->getMessage());
        }
    }
}
