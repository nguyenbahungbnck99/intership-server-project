<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Utils\SystemParam;
use App\Models\Combo;
use App\Models\ComboCourse;
use App\Models\Course;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ComboCourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 1) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $data = Combo::find($request->combo_id)->load('courses');
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 1) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $validator = Validator::make($request->all(), [
            'combo_id' => 'required',
            'course_id' => 'required',
        ], [
            'combo_id.required' => 'Vui lòng chọn combo',
            'course_id.required' => 'Vui lòng chọn khóa học',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }

        $combo = Combo::find($request->combo_id);
        if (!$combo) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::not_found_combo, '');
        }


        $courseId = $request->course_id;
        foreach ($courseId as $id) {
            $course = Course::find($id);
            if (!$course) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::not_found_course, '');
            }
        }
        if ($combo->courses)
            $combo->courses()->detach();
        $combo->courses()->attach($courseId);

        // $data = $request->all();
        // $create = ComboCourse::create($data);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $combo->load('courses','comboCourse'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function detail($id)
    {
        //
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 1) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $courseCourse = ComboCourse::find($id)->load('course','combo');
        if (!$courseCourse) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::not_found_course, null);
        }
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $courseCourse);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 1) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $validator = Validator::make($request->all(), [
            'combo_id' => 'required',
            'course_id' => 'required',
        ], [
            'combo_id.required' => 'Vui lòng chọn combo',
            'course_id.required' => 'Vui lòng chọn khóa học',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        $comboCourse = ComboCourse::find($id);
        if (!$comboCourse) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::not_found_comboCourse, '');
        }
        $combo = Combo::find($request->combo_id);
        if (!$combo) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::not_found_combo, '');
        }
        $course = Course::find($request->course_id);
        if (!$course) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::not_found_course, '');
        }
        $data = $request->all();

        $comboCourse->update($data);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $comboCourse);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        //
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 1) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $comboCourse = ComboCourse::find($id);
        if (!$comboCourse) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::not_found_comboCourse, null);
        }

        $comboCourse->delete();
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $comboCourse);
    }
}
