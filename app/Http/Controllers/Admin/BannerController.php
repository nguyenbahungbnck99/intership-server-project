<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Utils\SystemParam;
use App\Models\Slider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class BannerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $user = $this->getAuthenticatedUser();

        $where = [];
        // $where[] = ['is_active', 1];
        $data = Slider::where($where)->paginate(12);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
        $validator = Validator::make($request->all(), [
            'link' => 'required',
            'image' => 'required',
        ], [
            'link.required' => 'Vui lòng nhập link ',
            'image.required' => 'Vui lòng chọn ảnh',

        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        $data = $request->all();

        if ($request->hasFile('image')) {
            $filePath = SystemParam::saveImage($request->image,Slider::FOLDER);
            $data['image'] = $filePath;
        }

        $create = Slider::create($data);
        if ($create) {
            return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::create_success, $create);
        }
        return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::create_error, '');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    //
    public function active(Request $request, $id)
    {
        //

        $slider = Slider::findOrFail($id);

        $data = $request->all();
        $slider->update($data);

        if ($slider) {
            return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::update_success, $slider);
        }
        return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::create_error, '');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $data = Slider::findOrFail($id);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $validator = Validator::make($request->all(), [
            'link' => 'required',
            // 'image' => 'required',
        ], [
            'link.required' => 'Vui lòng nhập link ',
            // 'type.required' => 'Vui lòng chọn ảnh',

        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        $data = $request->all();
        $slider = Slider::findOrFail($id);
        if ($request->hasFile('image')) {
            $sliderQuery = DB::table('sliders')->where('id', $id)->first();
            // return $sliderQuery;
            SystemParam::deleteImage($sliderQuery->image);
            $filePath = SystemParam::saveImage($request->image,Slider::FOLDER);
            $data['image'] = $filePath;
        }

        $slider->update($data);
        if ($slider) {
            return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::update_success, $slider);
        }
        return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::update_error, '');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $feedback = Slider::findOrFail($id);
        $sliderQuery = DB::table('sliders')->where('id', $id)->first();
        SystemParam::deleteImage($sliderQuery->image);

        $feedback->delete();
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::delete_success, $feedback);
    }
}
