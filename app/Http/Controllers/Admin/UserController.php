<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\RequestLogin;
use App\Http\Requests\RequestRegisterCustomer;
use App\Http\Utils\SystemParam;
use App\Http\SpeedSMSAPI;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    public function register(RequestRegisterCustomer $request)
    {
        try {
            //code...
            $data = $request->all();
            $data['role_id'] = 1;
            // do {
            //     $data['code'] = rand(10000000, 99999999);
            // } while (User::where('code', $data['code'])->first());
            $user = User::create($data);
            return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::create_success, $user);
        } catch (\Throwable $th) {
            //throw $th;
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::create_error, '');
        }
    }
    public function login(Request $request)
    {
        $input = $request->only('phone', 'password', 'device_id');
        $data = [];
        if ($request->phone) {
            $where = [
                'phone' => $input['phone'],
                'role_id' => 1,
            ];
            $user = User::where($where)->first();
            if (!$user) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_gone, SystemParam::message_login_user_not_found, null);
            }

            $data = array(
                'email' => $user->email,
                'password' => $input['password'],
            );
            $token = null;
            if (!$token = JWTAuth::attempt($data, ['exp' => Carbon::now()->addDays(7)->timestamp])) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_unauthorized, SystemParam::message_login_error, null);
            }
            $data1 = array(
                'token' => $token,
                'user' => $user,
            );
            if ($request->device_id) {
                User::where([['phone', $input['phone']]])->update([
                    'device_id' => $request->device_id,
                ]);
            }
            return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::login_success, $data1);
        }
    }
    public function getUserInfo()
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::success, $user);
    }
    public function changePassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'current_password' => 'current_password',
            'password' => 'required|min:6|max:50|confirmed',
        ], [
            'current_password.current_password' => "Mật khẩu cũ không chính xác!",
            'password.required' => "Vui lòng nhập mật khẩu mới!",
            'password.min' => "Mật khẩu mới không dưới :min kí tự!",
            'password.max' => "Mật khẩu mới không quá :max kí tự!",
            'password.confirmed' => "Xác nhận mật khẩu mới không chính xác!",
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        $input = $request->only('password');
        $admin = $this->getAuthenticatedUser();
        if (!$admin) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        // if ($admin->role_id != 1) {
        //     return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::admin_not_found, null);
        // }
        $admin->update($input);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::update_password_success, $admin);
    }
    public function updateUser(Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        // if ($user->role_id != 1) {
        //     return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::admin_not_found, null);
        // }
        $validator = Validator::make($request->all(), [
            'name' => 'sometimes|string|max:255',
            'email' => 'sometimes|email',
        ], [
            'name.string' => 'Tên người dùng phải là kiểu chuỗi',
            'name.max' => 'Tên người dùng không quá :max kí tự',
            'email.email' => 'Vui lòng nhập email đúng định dạng',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        $req = $request->except(['role_id', 'phone']);
        if ($request->email && User::where([['id', '<>', $user->id], ['email', $request->email]])->exists()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_email_already_exist, null);
        }
        if ($request->birthday) {
            $req['birthday'] = date('Y-m-d', strtotime($request->birthday));
        }
        if ($request->file('avatar')) {
            $check_image = DB::table('users')->where('id', $user->id)->first();
            if (file_exists(ltrim($check_image->avatar, '/')) && $check_image->avatar != "") {
                unlink(ltrim($check_image->avatar, '/'));
            }
            $req['avatar'] = $this->uploadImage($request->file('avatar'));
        }
        $user->update($req);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::update_success, $user);
    }
    public function logout(Request $request)
    {
        $token = $request->header('Authorization');
        if (!$token) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_already_exist, SystemParam::error_already_exist, null);
        }
        JWTAuth::parseToken()->invalidate($token);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::logout_success, null);
    }
}
