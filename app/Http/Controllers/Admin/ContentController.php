<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Utils\SystemParam;
use App\Models\Category;
use App\Models\Contents;
use App\Models\ListImage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class ContentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $where = [];
        if ($request->category_id) {
            $where[] = ['category_id', $request->category_id];
        }
        if ($request->title) {
            $where[] = ['title', 'like', "%$request->title%"];
        }
        $data = Contents::where($where)->with('listImage','category')->paginate(SystemParam::PAGE_NUMBER);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::success, $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
        $user = $this->getAuthenticatedUser();

        $validator = Validator::make($request->all(), [
            'category_id' => 'required',
            'title' => 'required',
            'content' => 'required',
            // 'image' => 'required',

        ], [
            'category_id.required' => 'Vui lòng chọn danh mục.',
            'title.required' => 'Vui lòng nhập tiêu đề bài viết.',
            'content.required' => 'Vui lòng nhập nội dung bài viết',
            // 'image.required' => 'Vui lòng chọn ảnh',

        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        try {
            //code...

            $req = $request->all();
            $req['user_id'] = $user['id'];
            $req['slug'] = SystemParam::text_to_slug($request->title);
            $listImage = $request->file('image');

            $upload = $this->uploadArrayImage($listImage);

            $upload_rs = json_decode($upload);
            $create = Contents::create($req);

            if ($create) {
                foreach ($upload_rs as $key => $value) {
                    $image = [
                        'content_id' => $create['id'],
                        'image' => $value,
                    ];
                    ListImage::create($image);
                }

                return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::create_success, $create->load('listImage'));
            }
        } catch (\Throwable $th) {
            //throw $th;
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::create_error,  $th->getMessage());
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**delete the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $user = $this->getAuthenticatedUser();

        $validator = Validator::make($request->all(), [
            'category_id' => 'required',
            'title' => 'required',
            'content' => 'required',
            // 'image' => 'required',

        ], [
            'category_id.required' => 'Vui lòng chọn danh mục.',
            'title.required' => 'Vui lòng nhập tiêu đề bài viết.',
            'content.required' => 'Vui lòng nhập nội dung bài viết',
            // 'image.required' => 'Vui lòng chọn ảnh',

        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }
        try {
            //code...

            $category = Category::find($request->category_id);
            if ($category) {
                $req = [
                    'category_id' => $request->category_id,
                    'title' => $request->title,
                    'content' => $request->content,
                    'status' => $request->status,
                    'reviewer_id' => $user['id'],
                ];
                $req['slug'] = SystemParam::text_to_slug($request->title);
                $content = Contents::find($id);
                if ($request->hasFile('image')) {
                    $listImage = DB::table('list_image')->where('content_id', $id)->get();
                    foreach ($listImage as $image) {
                        SystemParam::deleteImage($image->image);
                    }
                    $content->listImage()->delete();
                    foreach ($request->image as $image) {
                        $filePath = SystemParam::saveImage($image, Contents::FOLDER);
                        $postImage = [];
                        $postImage['image'] = $filePath;
                        $postImage['content_id'] = $content->id;
                        ListImage::create($postImage);
                    }
                }
                if ($content) {
                    $content->update($req);
                    return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::update_success, $content->load('listImage'));
                }
            }
        } catch (\Throwable $th) {
            //throw $th;
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::update_error,  $th->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        //
        try {
            //code...

            $content = Contents::find($id);



            if (!$content) {
                return $this->responseApi(SystemParam::status_success, SystemParam::code_gone, SystemParam::gone, "");

            }
            $listImage = DB::table('list_image')->where('content_id', $id)->get();
            foreach ($listImage as $image) {
                SystemParam::deleteImage($image->image);
            }
            $content->listImage()->delete();
            $content->delete();
            return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::delete_success, $content);
        } catch (\Throwable $th) {
            //throw $th;
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::delete_error,  $th->getMessage());
        }
    }
}
