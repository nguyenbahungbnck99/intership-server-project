<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Utils\SystemParam;
use App\Models\Author;
use App\Models\Category;
use App\Models\Course;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class CourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 1) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $data = Course::paginate(12);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function listCourseAll()
    {
        //
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 1) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $data = Course::all();
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
        try {
            //code...

            $user = $this->getAuthenticatedUser();
            if (!$user) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
            }
            if ($user->role_id != 1) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
            }

            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'price' => 'required',
                'category_id' => 'required',
                'author_id' => 'required',
                'image' => 'required',
            ], [
                'name.required' => 'Vui lòng nhập tên khóa học',
                'price.required' => 'Vui lòng nhập giá',
                'category_id.required' => 'Vui lòng nhập tên danh mục',
                'author_id.required' => 'Vui lòng nhập tác giả',
                'image.required' => 'Vui lòng chọn ảnh',
            ]);
            if ($validator->fails()) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
            }

            $checkCategory = Category::find($request->category_id);
            if (!$checkCategory) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::not_found_category, '');
            }
            $checkAuthor = Author::find($request->author_id);
            if (!$checkAuthor) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::not_found_author, '');
            }
            $data = $request->all();
            if ($request->hasFile('image')) {
                $filePath = SystemParam::saveImage($request->image, Course::FOLDER);
                $data['image'] = $filePath;
            }
            $data['slug'] = SystemParam::text_to_slug($request->name);
            $data['price'] = SystemParam::cleanString($request->price);
            $create = Course::create($data);

            return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::create_success, $create);
        } catch (\Throwable $th) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::create_error, $th->getMessage());
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function detail($id)
    {
        //
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 1) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }
        $course = Course::find($id);
        if (!$course) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::not_found_course, null);
        }
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $course);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
        }
        if ($user->role_id != 1) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
        }

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'price' => 'required',
            'category_id' => 'required',
            'author_id' => 'required',
        ], [
            'name.required' => 'Vui lòng nhập tên khóa học',
            'price.required' => 'Vui lòng nhập giá',
            'category_id.required' => 'Vui lòng nhập tên danh mục',
            'author_id.required' => 'Vui lòng nhập tác giả',
        ]);
        if ($validator->fails()) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
        }

        $checkCategory = Category::find($request->category_id);
        if (!$checkCategory) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::not_found_category, '');
        }
        $checkAuthor = Author::find($request->author_id);
        if (!$checkAuthor) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::not_found_author, '');
        }
        $course = Course::find($id);
        if (!$course) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::not_found_course, '');
        }
        $data = $request->all();
        if ($request->hasFile('image')) {
            $courseQuery = DB::table('course')->where('id', $id)->first();
            SystemParam::deleteImage($courseQuery->image);
            $filePath = SystemParam::saveImage($request->image, Course::FOLDER);
            $data['image'] = $filePath;
        }
        $data['slug'] = SystemParam::text_to_slug($request->name);
        $data['price'] = SystemParam::cleanString($request->price);
        $course->update($data);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::update_success, $course);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        //
        try {
            //code...
            $user = $this->getAuthenticatedUser();
            if (!$user) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
            }
            if ($user->role_id != 1) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
            }
            $course = Course::find($id);
            if (!$course) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_not_found, SystemParam::not_found_course, null);
            }
            $authQuery = DB::table('course')->where('id', $id)->first();
            SystemParam::deleteImage($authQuery->image);
            $course->delete();
            return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::delete_success, $course);
        } catch (\Throwable $th) {
            //throw $th;
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::delete_error, '');
        }
    }
}
