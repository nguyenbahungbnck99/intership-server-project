<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use App\Http\Utils\SystemParam;
use App\Models\Combo;
use App\Models\Course;
use Illuminate\Http\Request;

class ComboController extends Controller
{
    //
    public function listCombo()
    {
        $listComboBasic = Combo::where('status', Course::BASIC)->get()->load('courses');
        $listComboAdvance = Combo::where('status', Course::ADVANCE)->get()->load('courses');
        $data = [
            'listComboBasic' => $listComboBasic,
            'listComboAdvance' => $listComboAdvance,
        ];
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }


    public function comboDetail($slug)
    {
        $data = Combo::where('slug',$slug)->with('courses.author','courses.category')->first();
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
}
