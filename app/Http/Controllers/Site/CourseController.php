<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use App\Http\Utils\SystemParam;
use App\Models\Comment;
use App\Models\Course;
use App\Models\Lesson;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CourseController extends Controller
{
    //
    public function listCourseBasic()
    {
        $data = Course::where('status', Course::BASIC)->withCount('lesson')->get()->load('author', 'category');
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function listCourseAdvance()
    {
        $data = Course::where('status', Course::ADVANCE)->withCount('lesson')->get()->load('author', 'category');
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function courseDetail($slug)
    {
        $data = Course::where('slug', $slug)->first()->load('author', 'category', 'lesson','comments');
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function lesson($slug)
    {
        // $lesson = Lesson::where('slug', $slug)->with('course.comments', function ($query) {
        //     return $query->where('status',  1);
        // })->first()->load('course.lesson');
        $lesson = Lesson::where('slug', $slug)->first()->load('course.lesson','course.comments');
        // $course = Course::find($lesson->cour)->load('author','category','lesson');
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $lesson);
    }

    public function commentCourse(Request $request)
    {
        //
        try {
            //code...

            // $user = $this->getAuthenticatedUser();
            // if (!$user) {
            //     return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::token_expired, null);
            // }
            // if ($user->role_id != 1) {
            //     return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::error_not_role, null);
            // }

            $validator = Validator::make($request->all(), [
                'course_id' => 'required',
                'user_id' => 'required',
                'content' => 'required',
            ], [
                'course_id.required' => 'Vui lòng nhập  khóa học',
                'user_id.required' => 'Vui lòng nhập user_id',
                'content.required' => 'Vui lòng nhập bình luận',
            ]);
            if ($validator->fails()) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
            }

            $data = $request->all();

            $create = Comment::create($data);

            return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::create_success, $create);
        } catch (\Throwable $th) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::create_error, $th->getMessage());
        }
    }
}
