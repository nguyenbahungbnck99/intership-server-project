<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use App\Http\Requests\CustomerRegisterRequest;
use App\Http\Requests\RequestRegisterCustomer;
use App\Http\Utils\SystemParam;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Facades\JWTAuth;

class UserController extends Controller
{
    //
    public function register(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'phone' => 'required|regex:/^0[0-9]{9,11}+$/|unique:users',
                'email' => 'required|email|unique:users',
                'name' => 'required|string|max:50',
                'password' => 'required',
                'confirm_password' => 'required|same:password',
            ], [
                'phone.required' => 'Số điện thoại chưa được nhập!',
                'phone.regex' => 'Số điện thoại không đúng định dạng!',
                'phone.unique' => 'Số điện thoại đã được đăng ký!',
                'email.required' => 'Email chưa được nhập!',
                'email.unique' => 'Email đã được đăng ký!',
                'email.email' => 'Email không đúng định dạng',
                'name.required' => 'Tên chưa được nhập!',
                'password.required' => 'Mật khẩu chưa được nhập!',
                'confirm_password.required' => 'Xác nhận mật khẩu chưa được nhập!',
                'confirm_password.same' => 'Xác nhận mật khẩu không trùng khớp!',
            ]);
            if ($validator->fails()) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_error_validate, $validator->getMessageBag()->first(), $validator->errors());
            }
            //code...
            $data = $request->all();
            $data['role_id'] = 5;
            // do {
            //     $data['code'] = rand(10000000, 99999999);
            // } while (User::where('code', $data['code'])->first());
            $user = User::create($data);
            return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::register_success, $user);
        } catch (\Throwable $th) {
            //throw $th;
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_server, SystemParam::register_error, $th->getMessage());
        }
    }
    public function login(Request $request)
    {
        $input = $request->only('phone', 'password', 'device_id');
        $data = [];
        if ($request->phone) {
            $where = [
                'phone' => $input['phone'],
                'role_id' => 5,
            ];
            $user = User::where($where)->first();
            if (!$user) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_gone, SystemParam::message_login_user_not_found, null);
            }

            $data = array(
                'email' => $user->email,
                'password' => $input['password'],
            );
            $token = null;
            if (!$token = JWTAuth::attempt($data, ['exp' => Carbon::now()->addDays(7)->timestamp])) {
                return $this->responseApi(SystemParam::status_error, SystemParam::code_gone, SystemParam::message_login_error, null);
            }
            $data1 = array(
                'token' => $token,
                'user' => $user,
            );
            if ($request->device_id) {
                User::where([['phone', $input['phone']]])->update([
                    'device_id' => $request->device_id,
                ]);
            }
            return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::login_success, $data1);
        }
    }
    public function logout(Request $request)
    {
        $token = $request->header('Authorization');
        if (!$token) {
            return $this->responseApi(SystemParam::status_error, SystemParam::code_error_already_exist, SystemParam::error_already_exist, null);
        }
        JWTAuth::parseToken()->invalidate($token);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, SystemParam::logout_success, null);
    }
}
