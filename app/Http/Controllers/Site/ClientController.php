<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use App\Http\Utils\SystemParam;
use App\Models\Category;
use App\Models\CategoryProduct;
use App\Models\Certificate;
use App\Models\Contents;
use App\Models\Feedback;
use App\Models\Product;
use App\Models\Setting;
use App\Models\Slider;
use App\Models\ViewProductShare;
use Illuminate\Http\Request;

class ClientController extends Controller
{
    //

    public function shareProduct($id)
    {
        $data = Product::findOrFail($id)->load('productFeel', 'relatedProduct.product', 'productImage', 'productSlider', 'productVideo', 'productRecommend', 'productPromotion', 'productFeel', 'productContent', 'productQuestion', 'productContent2', 'productAdvantage');
        if ($data) {
            ViewProductShare::create(['product_id' => $id]);
        }
        $slider = Slider::where('is_active', 1)
            ->orderBy('id', 'desc')
            ->get();
        $rs = [
            'product' => $data,
            'slider' => $slider,
        ];
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $rs);
    }


    public function listProduct(Request $request)
    {
        $data = Product::orderby('id', 'desc')
            ->whereNotNull('slug')
            ->paginate(25);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function listProductAll(Request $request)
    {
        $data = Product::paginate(12);
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }

    public function listCategoryProduct(Request $request)
    {
        $data = CategoryProduct::select('id', 'name', 'slug')->get();
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function listCategoryContent(Request $request)
    {
        $data = Category::with('children')
            ->where('parent', 0)
            ->get();
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function listContentByCategorySlug($category_slug)
    {
        $where = [];
        $where[] = ['status', 1];
        $category = Category::where('slug', $category_slug)->first();
        $where[] = ['category_id', $category->id];
        $postNew = Contents::where($where)
            ->with('listImage')
            // ->take(3)
            ->orderBy('id', 'desc')
            ->get();
        // $listContent = Contents::where($where)
        //     ->whereNotIn('id',  $postNew->pluck('id')->toArray())
        //     ->with('user', 'listImage')
        //     ->get();
        $data = [
            'postNew' => $postNew,
            'category' => $category,
            // 'listContent' => $listContent,
        ];
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function detailContent($content_slug)
    {
        $content = Contents::with('category')
            ->where('slug', $content_slug)
            ->first();
        $postRelated = Contents::where('id', '!=', $content->id)
            ->with('listImage')
            ->take(12)
            ->orderBy('id', 'desc')
            ->get();
        $data = [
            'content' => $content,
            'postRelated' => $postRelated,
        ];
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function listHotProduct(Request $request)
    {
        // $data = Product::orderBy('total_sold','desc')->take(4)->get()->load('categoryProduct');
        $data = Product::orderBy('total_sold', 'desc')
            ->take(6)
            ->whereNotNull('product.slug')
            ->select('id', 'slug', 'name', 'image', 'price', 'price_sale')
            ->get();
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function listProductByCategory(Request $request, $category_slug)
    {
        $data = Product::join('category_products', 'category_products.id', 'product.category_id')
            ->where('category_products.slug', $category_slug)
            ->whereNotNull('product.slug')
            ->select('product.id', 'product.slug', 'product.name', 'product.image', 'product.price', 'product.price_sale')
            ->get();
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function search(Request $request)
    {
        $data = Product::where('name', 'like', "%$request->key_search%")
            ->whereNotNull('product.slug')
            ->select('product.id', 'product.slug', 'product.name', 'product.image', 'product.price', 'product.price_sale')
            ->get();
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function listAllProduct(Request $request)
    {
        $data = Product::orderBy('total_sold', 'desc')
            ->whereNotNull('product.slug')
            ->select('id', 'slug', 'name', 'image', 'price', 'price_sale')
            ->get();
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function getNewProduct(Request $request)
    {
        // $data = Product::orderBy('total_sold','desc')->take(4)->get()->load('categoryProduct');
        $data = Product::orderBy('id', 'desc')
            ->whereNotNull('slug')
            ->select('id', 'slug', 'name', 'image', 'description')
            ->first();
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function productDetailBySlug($slug)
    {
        $data = Product::where('slug', $slug)
            ->first()
            ->load(
                'productFeel',
                'relatedProduct.product',
                'productImage'
            );
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function getTwoCertificate()
    {
        $data = Certificate::with('certificateItem')
            ->orderBy('id', 'desc')
            ->take(2)
            ->get();
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function listCertificate()
    {
        $data = Certificate::with('certificateItem')
            ->orderBy('id', 'desc')
            ->get();
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function detailCertificate($certificate_slug)
    {
        $data = Certificate::where('slug', $certificate_slug)
            ->first()
            ->load('certificateItem');
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function feedbackHot()
    {
        $data = Feedback::where('is_hot', 1)
            ->orderBy('id', 'desc')
            ->get();
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function listFeedback()
    {
        $data = Feedback::orderBy('id', 'desc')->get();
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function listSlider()
    {
        $data = Slider::where('is_active', 1)
            ->orderBy('id', 'desc')
            ->get();

        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function getProductRelated($product_id)
    {
        $product = Product::findOrFail($product_id);
        $data = Product::where('id', '!=', $product_id)
            ->where('category_id', $product->category_id)
            ->get();
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function listProductMilk()
    {
        $key = 'sua';
        // $roleMilk = CategoryProduct::where('name','like',"%$key%")->get()->pluck('id');

        // $data = Product::whereIn('category_id', $roleMilk)->get();
        $data = Product::where('name', 'like', "%$key%")->get();
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function listProductFunctionalFoods()
    {
        $key = 'sua';
        // $roleMilk = CategoryProduct::where('name','like',"%$key%")->get()->pluck('id');
        // $data = Product::whereNotIn('category_id', $roleMilk)->get();
        $data = Product::where('name', 'not like', "%$key%")->get();

        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }
    public function getInforWebsite()
    {
        $data = Setting::getInfor();

        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }







    public function productDetail($id)
    {
        $data = Product::findOrFail($id)->load('productFeel', 'relatedProduct.product', 'productImage');
        return $this->responseApi(SystemParam::status_success, SystemParam::code_success, '', $data);
    }

}

