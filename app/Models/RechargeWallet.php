<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class RechargeWallet extends Model
{
    //
    protected $table = 'recharge_wallets';
    protected $fillable = [
        'user_id',
        'name',
        'total_money',
    ];
    public function rechargeHistory()
    {
        return $this->hasMany(RechargeHistory::class, 'recharge_wallet_id');
    }
    public function userRechargeWallet()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
    public static function boot()
    {
        parent::boot();

        static::deleting(function ($rechargeWallet) { // before delete() method call this
            $rechargeWallet->rechargeHistory()->delete();
            // do the rest of the cleanup...
        });
    }
}
