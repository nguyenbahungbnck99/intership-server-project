<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DetailOrder extends Model
{
    //
    protected $table = 'detail_order';
    protected $primaryKey = 'id';
    protected $fillable = [
        'order_id',
        'product_id',
        'product_name',
        'number',
        'price',
    ];
  
    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }
    public function order(){
        return $this->belongsTo(Order::class, 'order_id');
    }
}
