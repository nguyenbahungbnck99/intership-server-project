<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
    const FOLDER = 'uploads/images/sliders/';

    protected $table = 'sliders';
    protected $fillable = [
        'image',
        'link',
        'title',
        'content',
        'is_active',
    ];
    public static function getImageAttribute($image)
    {
        if ($image != null) {
            $image = asset($image);
        }
        return $image;
    }
}
