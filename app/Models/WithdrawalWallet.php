<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;
class WithdrawalWallet extends Model
{
    //
    protected $table = 'withdrawal_wallets';
    protected $fillable = [
        'user_id',
        'name',
        'total_money',
    ];
    public function withdrawalHistory()
    {
        return $this->hasMany(WithdrawalHistory::class, 'withdrawal_wallet_id');
    }
    public function delete()
    {
        $this->withdrawalHistory()->delete();

        return parent::delete();
    }
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public static function boot()
    {
        parent::boot();

        static::deleting(function ($rechargeWallet) { // before delete() method call this
            $rechargeWallet->withdrawalHistory()->delete();
            // do the rest of the cleanup...
        });
    }
}
