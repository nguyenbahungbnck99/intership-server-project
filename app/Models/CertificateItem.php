<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CertificateItem extends Model
{
    //
    protected $table = 'certificate_items';
    protected $fillable = [
        'certificate_id',
        'name',
        'title',
        'item_image',
        'description',
        'image',
    ];
    // public function certificate() {
    //     return $this->belongsTo(Certificate::class);
    // }
    public static function getImageAttribute($image)
    {
        if ($image != null) {
            $image = asset($image);
        }
        return $image;
    }
    public static function getItemImageAttribute($image)
    {
        if ($image != null) {
            $image = asset($image);
        }
        return $image;
    }
}
