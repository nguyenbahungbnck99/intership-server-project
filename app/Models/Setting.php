<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    //
    const FOLDER = 'uploads/setting/';

    const TYPE_EMAIL = 1;
    const TYPE_PHONE = 2;
    const TYPE_FB_REF = 3;
    const TYPE_FB_PAGE_ID = 4;
    const TYPE_LOGO = 5;
    const TYPE_INTRODUCE = 6;
    const TYPE_COPYRIGHT = 7;
    const TYPE_ADDRESS = 8;
    const EMAIL = "healthmoomy@gmail.com";
    const PHONE = "0373102386";
    const FB_REF = "https://www.facebook.com/SHOP-M%E1%BA%B8-T%C3%8D-109946358008365/?ref=page_internal";
    const FB_PAGE_ID = "100460761501507";
    const INTRODUCE = "Đổi giới thiệu công ty Healthmoomy. Healthmoomy chuyên phân phối các dòng sản phẩm dinh dưỡng cho mẹ bầu và bé. Để đảm bảo dương chất đầy đủ cho mẹ và bé thì cần những vi chất cần thiết như đạm, kẽm, máu để mẹ và bé có sức khỏe tốt nhất";
    const COPYRIGHT = "2021, CH APP & Developed by CH APP";
    const ADDRESS = "Số 49, ngõ 1 Lê Văn Thiêm, Nhân Chính, Thanh Xuân, Hà Nội.";
    protected $table = 'settings';
    protected $fillable = [
        'type',
        'content',
    ];
    public static function logoDefault(){
        return asset("images/logo.png");
    }
    public static function getInfor(){
        $email = Setting::where('type',Setting::TYPE_EMAIL)->first('content');
        $phone = Setting::where('type',Setting::TYPE_PHONE)->first('content');
        $fb_ref = Setting::where('type',Setting::TYPE_FB_REF)->first('content');
        $fb_page_id= Setting::where('type',Setting::TYPE_FB_PAGE_ID)->first('content');
        $introduce = Setting::where('type',Setting::TYPE_INTRODUCE)->first('content');
        $logo = Setting::where('type',Setting::TYPE_LOGO)->first('content');
        $copyright = Setting::where('type',Setting::TYPE_COPYRIGHT)->first('content');
        $address = Setting::where('type',Setting::TYPE_ADDRESS)->first('content');
        $email =!empty($email) ? $email['content'] : self::EMAIL;
        $phone = !empty($phone) ? $phone['content'] : self::PHONE;
        $fb_ref =!empty($fb_ref) ?  $fb_ref['content'] : self::FB_REF;
        $fb_page_id =!empty($fb_page_id) ?  $fb_page_id['content'] : self::FB_PAGE_ID;
        $introduce =!empty($introduce) ? $introduce['content'] : self::INTRODUCE;
        $logo =!empty($logo) ?  asset($logo['content']) : self::logoDefault();
        $copyright =!empty($copyright) ?  $copyright['content'] : self::COPYRIGHT;
        $address =!empty($address) ?  $address['content'] : self::ADDRESS;
        return [
            'email' => $email,
            'phone' => $phone,
            'fb_ref' => $fb_ref,
            'fb_page_id' => $fb_page_id,
            'logo' => $logo,
            'introduce' => $introduce,
            'copyright' => $copyright,
            'address' => $address,
        ];
    }
}
