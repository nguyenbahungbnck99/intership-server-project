<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RoseCtv extends Model
{
    //
    protected $table = 'rose_ctv';
    protected $fillable = [
        'total_money_ctv',
        'rose_ctv',
    ];
}
