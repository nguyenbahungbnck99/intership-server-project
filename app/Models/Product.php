<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //
    const FOLDER = 'uploads/images/products/';

    protected $table = 'product';
    protected $primaryKey = 'id';
    protected $fillable = [
        'name',
        'slug',
        'price',
        'price_sale',
        'image',
        'description',
        'content',
        'strategic_vision',
        'user_object',
        'user_manual',
        'usefulness',
        'category_id',
        'total_inventory',
        'total_sold',
    ];
    public function getImageAttribute($image)
    {
        if ($image == null) {
            return asset('/image_default/avatar_default.jpg');
        }
        return asset($image);
    }
    public function content2Product()
    {
        return $this->HasOne(Content2Product::class, 'product_id');
    }
    public function viewProductShare()
    {
        return $this->HasMany(ViewProductShare::class, 'product_id');
    }
    public function productFeel()
    {
        return $this->HasMany(ProductFeel::class, 'product_id');
    }
    public function productImage()
    {
        return $this->HasMany(ProductImage::class, 'product_id');
    }
    public function productQuestion()
    {
        return $this->HasMany(ProductQuestion::class, 'product_id');
    }
    public function productAdvantage()
    {
        return $this->HasMany(ProductAdvantage::class, 'product_id');
    }
    public function productContent()
    {
        return $this->HasMany(ProductContent::class, 'product_id');
    }
    public function productContent2()
    {
        return $this->HasMany(ProductContent2::class, 'product_id');
    }
    public function categoryProduct()
    {
        return $this->belongsTo(CategoryProduct::class, 'category_id');
    }
    public function relatedProduct()
    {
        return $this->HasMany(RelatedProduct::class, 'product_id');
    }
    public function productSlider()
    {
        return $this->HasMany(ProductSlider::class, 'product_id');
    }
    public function productVideo()
    {
        return $this->HasMany(ProductVideo::class, 'product_id');
    }
    public function productRecommend()
    {
        return $this->HasMany(ProductRecommend::class, 'product_id');
    }
    public function productPromotion()
    {
        return $this->HasMany(ProductPromotion::class, 'product_id');
    }
    public function delete()
    {

        $this->content2Product()->delete();

        $this->viewProductShare()->delete();
        $this->productFeel()->delete();
        $this->productImage()->delete();
        $this->productQuestion()->delete();
        $this->productAdvantage()->delete();
        $this->productContent()->delete();
        $this->productContent2()->delete();
        $this->relatedProduct()->delete();
        $this->productSlider()->delete();
        $this->productVideo()->delete();
        $this->productRecommend()->delete();
        $this->productPromotion()->delete();
        // delete the user
        return parent::delete();
    }
}
