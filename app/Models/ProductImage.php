<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductImage extends Model
{
    //
    protected $table = 'image_product';
    protected $fillable = [
        'product_id',
        'image'
    ];
    public function product() {
        return $this->belongsTo(Product::class);
    }
    public static function getImageAttribute($image)
    {
        if ($image != null) {
            $image = asset($image);
        }
        return $image;
    }
}
