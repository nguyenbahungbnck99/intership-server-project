<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'category';
    protected $primaryKey = 'id';
    protected $fillable = [
        'name',
        'slug',
        'parent',
    ];
    public function children()
    {
        return $this->hasMany(Category::class, 'parent', 'id');
    }
    public function contents()
    {
        return $this->hasMany(Contents::class, 'category_id');
    }
    public function delete()
    {

        $this->children()->delete();

        return parent::delete();
    }
}
