<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductAdvantage extends Model
{
    //
    protected $table = 'product_advantage';
    protected $primaryKey = 'id';
    protected $fillable = [
        'product_id',
        'content'
    ];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
