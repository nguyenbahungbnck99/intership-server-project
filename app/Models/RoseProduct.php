<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RoseProduct extends Model
{
    //
    protected $table = 'rose_product';
    protected $fillable = [
        'type_ctv',
        'rose_money',
    ];
}
