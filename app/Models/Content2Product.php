<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Content2Product extends Model
{
    protected $table = 'content2product';
    protected $primaryKey = 'id';
    protected $fillable = [
        'product_id',
        'image',
    ];

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }

    public function detailContent2()
    {
        return $this->hasMany(detailContent2::class, 'content_id');
    }
 
    public function getImageAttribute($image)
    {
        if ($image == null) {
            return asset('/image_default/view_default.jpg');
        }
        return asset($image);
    }

}
