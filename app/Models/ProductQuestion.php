<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductQuestion extends Model
{
    //
    protected $table = 'question_product';
    protected $primaryKey = 'id';
    protected $fillable = [
        'product_id',
        'content',
        'image'
    ];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public static function getImageAttribute($image)
    {
        if ($image != null) {
            $image = asset($image);
        }
        return $image;
    }
}
