<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class detailContent2 extends Model
{
    protected $table = 'detail_content2_product';
    protected $primaryKey = 'id';
    protected $fillable = [
        'content_id',
        'content',
    ];

    public function content()
    {
        return $this->belongsTo(Content2Product::class, 'content_id');
    }
 
}
