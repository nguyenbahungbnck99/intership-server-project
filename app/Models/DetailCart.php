<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DetailCart extends Model
{
    protected $table = 'detail_cart';
    protected $primaryKey = 'id';
    protected $fillable = [
        'user_id',
        'product_id',
        'product_name',
        'number',
        'price',
    ];
 
    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }
    public function User()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
