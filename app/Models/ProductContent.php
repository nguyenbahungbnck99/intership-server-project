<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductContent extends Model
{
    //
    protected $table = 'product_contents';
    protected $primaryKey = 'id';
    protected $fillable = [
        'product_id',
        'title',
        'description',
        'content',
    ];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
