<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RechargeHistory extends Model
{
    //
    protected $table = 'recharge_histories';
    protected $fillable = [
        'recharge_wallet_id',
        'account_name',
        'account_stk',
        'money',
        'content',
        'image',
        'branch',
        'bank_name',
        'status'
    ];
    public function rechargeWallet()
    {
        return $this->belongsTo(RechargeWallet::class);
    }
    public function getImageAttribute($image)
    {
        if ($image == null) {
            return asset('/image_default/avatar_default.jpg');
        }
        return asset($image);
    }

    public function historyUser()
    {
        return $this->hasOneThrough(
            // car::class,
            // owner::class,
            User::class,
            RechargeWallet::class,
            'user_id', // Foreign key on the cars table...
            'withdrawal_wallet_id', // Foreign key on the owners table...
            'id', // Local key on the mechanics table...
            'id' // Local key on the cars table...
        );
    }
}
