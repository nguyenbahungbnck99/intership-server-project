<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Product;
class RelatedProduct extends Model
{
    //
    protected $table = 'related_product';
    protected $primaryKey = 'id';
    protected $fillable = [
        'product_id',
        'related_product_id'
    ];
    public function product()
    {
        return $this->belongsTo(Product::class, 'related_product_id');
    }
}
