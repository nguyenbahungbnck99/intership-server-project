<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Certificate extends Model
{
    //
    protected $table = 'certificates';
    protected $fillable = [
        'name',
        'slug',
        'slogan',
        'description',
        'slogan_2',
        'description_2',
        'image',
    ];
    public function certificateItem(){
        return $this->hasMany(CertificateItem::class,'certificate_id');
    }
    public static function getImageAttribute($image)
    {
        if ($image != null) {
            $image = asset($image);
        }
        return $image;
    }

}
