<?php

namespace App\Models;
use App\User;
use Illuminate\Database\Eloquent\Model;
use phpDocumentor\Reflection\DocBlock\Tags\Uses;

class Contents extends Model
{
    const FOLDER = 'uploads/images/contents/';
    protected $table = 'contents';
    protected $primaryKey = 'id';
    protected $fillable = [
        'name',
        'title',
        'slug',
        'user_id',
        'category_id',
        'title',
        'content',
        'status',
        'reviewer_id'
    ];

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
    public function reviewer()
    {
        return $this->belongsTo(User::class, 'reviewer_id');
    }
    public function listImage()
    {
        return $this->hasMany(ListImage::class, 'content_id');
    }
    public function getCreatedAtAttribute($created_at)
    {

        return date('d-m-Y H:i',strtotime($created_at));
    }

    public function delete()
    {

        $this->listImage()->delete();

        return parent::delete();
    }
}
