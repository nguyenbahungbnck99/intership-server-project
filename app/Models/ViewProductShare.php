<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ViewProductShare extends Model
{
    //
    protected $table = 'view_product_shares';
    protected $fillable = [
        'product_id'
    ];
}
