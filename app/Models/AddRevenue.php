<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;
class AddRevenue extends Model
{
    protected $table = 'add_revenue';
    protected $fillable = [
        'user_id',
        'price',
        'user_create_id',
        'content',
        'status',
    ];
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
    public function userCreate()
    {
        return $this->belongsTo(User::class, 'user_create_id');
    }
}
