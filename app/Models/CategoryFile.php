<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CategoryFile extends Model
{
    protected $table = 'category_file';
    protected $primaryKey = 'id';
    protected $fillable = [
        'name',
        'image',
    ];
    public function getImageAttribute($image)
    {
        if ($image == null) {
            return null;
        }
        return asset($image);
    }
    public function file()
    {
        return $this->hasMany(File::class, 'category_file_id');
    }
}
