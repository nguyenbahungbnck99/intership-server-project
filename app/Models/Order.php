<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;
class Order extends Model
{
    //
    protected $table = 'order';
    protected $primaryKey = 'id';
    protected $fillable = [
        'user_id',
        'name_user',
        'phone_order',
        'address_order',
        'total_product',
        'total_money_product',
        'content',
        'ship',
        'status',
        'type_ctv',
        'date_order',
        'image_bill',
    ];
    public function getImageAttribute($image_bill)
    {
        if ($image_bill == null) {
            return asset('/image_default/avatar_default.jpg');
        }
        return asset($image_bill);
    }
    public function User()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
    public function DetailOrder()
    {
        return $this->hasMany(DetailOrder::class, 'order_id');
    }
    public function bill()
    {
        return $this->hasOne(Bill::class);
    }
    
}
