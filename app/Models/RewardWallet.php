<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;
class RewardWallet extends Model
{
    //
    protected $table = 'reward_wallet';
    protected $fillable = [
        'user_id',
        'name',
        'total_money',
    ];
    public function rewardlHistory()
    {
        return $this->hasMany(RewardHistory::class, 'reward_wallet_id');
    }
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
