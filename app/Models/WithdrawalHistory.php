<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WithdrawalHistory extends Model
{
    //
    protected $table = 'withdrawal_histories';
    protected $fillable = [
        'withdrawal_wallet_id',
        'account_name',
        'account_stk',
        'money',
        'content',
        'image',
        'branch',
        'bank_name',
        'status'
    ];
    public function withdrawalWallet()
    {
        return $this->belongsTo(WithdrawalWallet::class);
    }
    public function getImageAttribute($image)
    {
        if ($image == null) {
            return asset('/image_default/avatar_default.jpg');
        }
        return asset($image);
    }
}
