<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;
class BuyPacket extends Model
{
    protected $table = 'buy_packet';
    protected $fillable = [
        'name',
        'price',
        'phone',
        'packet_id',
        'address',
        'content',
        'code_ctv',
        'content_payment',
        'status',
        'user_id'
    ];
    public function packet()
    {
        return $this->belongsTo(Packet::class, 'packet_id');
    }
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
    public function getImageAttribute($image)
    {
        if ($image == null) {
            return null;
        }
        return asset($image);
    }
}
