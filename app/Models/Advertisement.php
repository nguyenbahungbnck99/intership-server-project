<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Advertisement extends Model
{
    protected $table = 'advertisement';
    protected $primaryKey = 'id';
    protected $fillable = [
        'name',
        'content',
        'description',
        'main_image',
        'list_image',
    ];
    public function getMainImageAttribute($main_image)
    {
        if ($main_image == null) {
            return null;
        }
        return asset($main_image);
    }
    public function getListImageAttribute($list_image)
    {
        $array = [];
        if($list_image != null){
            $data = json_decode($list_image);
            foreach($data as $item){
                $array[] = asset($item);
            }
        }
        return $array;
    }
}
