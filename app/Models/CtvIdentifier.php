<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CtvIdentifier extends Model
{
    //
    protected $table = 'ctv_identifier';
    protected $fillable = [
        'name',
        'price',
        'share_number',
        'money',
    ];
}
