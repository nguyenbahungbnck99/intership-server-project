<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    protected $table = 'file';
    protected $primaryKey = 'id';
    protected $fillable = [
        'name',
        'image',
        'file',
        'category_file_id'
    ];
    public function getImageAttribute($image)
    {
        if ($image == null) {
            return null;
        }
        return asset($image);
    }
    public function getFileAttribute($file)
    {
        if ($file == null) {
            return null;
        }
        return asset($file);
    }
    public function categoryFile()
    {
        return $this->belongsTo(CategoryFile::class, 'category_file_id');
    }
}
