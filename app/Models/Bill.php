<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Bill extends Model
{
    //
    protected $table = 'bills';
    protected $fillable = [
        'order_id',
        'code_bill',
        'money',
        'image',
        'source_account',
        'source_user_name',
        'destination_account',
        'transaction_date',
    ];
}
