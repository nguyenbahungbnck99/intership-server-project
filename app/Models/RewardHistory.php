<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RewardHistory extends Model
{
    //
    protected $table = 'reward_history';
    protected $fillable = [
        'reward_wallet_id',
        'account_name',
        'account_stk',
        'money',
        'content',
        'image',
        'branch',
        'bank_name',
        'status',
        'type',
        'order_id'
    ];
    public function rewardWallet()
    {
        return $this->belongsTo(RewardWallet::class);
    }
    public function order()
    {
        return $this->belongsTo(Order::class);
    }
    public function getImageAttribute($image)
    {
        if ($image == null) {
            return asset('/image_default/avatar_default.jpg');
        }
        return asset($image);
    }
}
