<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductFeel extends Model
{
    //
    protected $table = 'product_feel';
    protected $primaryKey = 'id';
    protected $fillable = [
        'product_id',
        'avatar',
        'name',
        'age_location',
        'content'
    ];
    public function getAvatarAttribute($avatar)
    {
        if ($avatar == null) {
            return asset('/image_default/avatar_default.jpg');
        }
        return asset($avatar);
    }
}
