<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductPromotion extends Model
{
    //
    protected $table = 'product_promotions';
    protected $fillable = [
        'product_id',
        'title',
        'image',
    ];
    public static function getImageAttribute($image)
    {
        if ($image != null) {
            $image = asset($image);
        }
        return $image;
    }
}
