<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductRecommend extends Model
{
    //
    protected $table = 'product_recommends';
    protected $fillable = [
        'product_id',
        'link',
        'image',
    ];
    public static function getImageAttribute($image)
    {
        if ($image != null) {
            $image = asset($image);
        }
        return $image;
    }
}
