<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductContent2 extends Model
{
    protected $table = 'product_content2';
    protected $primaryKey = 'id';
    protected $fillable = [
        'product_id',
        'content',
        'image',
    ];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function getImageAttribute($image)
    {
        if ($image == null) {
            return asset('/image_default/view_default.jpg');
        }
        return asset($image);
    }
}
