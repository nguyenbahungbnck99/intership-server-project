<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CategoryProduct extends Model
{
    //
    protected $table = 'category_products';
    protected $fillable = [
        'name',
        'slug',
        'parent',
    ];
    public function children()
    {
        return $this->hasMany(CategoryProduct::class, 'parent', 'id');
    }
    public function products()
    {
        return $this->hasMany(Product::class, 'category_id');
    }
    public function delete()
    {

        $this->children()->delete();

        return parent::delete();
    }


}
